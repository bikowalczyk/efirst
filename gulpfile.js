var gulp = require("gulp"),
  inlineCss = require("gulp-inline-css");

const autoprefixer = require("gulp-autoprefixer");
const rename = require("gulp-rename");
const cleanCSS = require("gulp-clean-css");

gulp.task("prefix-css", () =>
  gulp
    .src("Zadanie_2/css/main.css")
    .pipe(rename("css/main-prefixed.css"))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(gulp.dest("Zadanie_2/"))
);

gulp.task("minify-css", () => {
  return gulp
    .src("Zadanie_2/css/main-prefixed.css")
    .pipe(rename("css/main-min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(gulp.dest("Zadanie_2/dist"));
});

gulp.task("default", function() {
  return gulp
    .src("Zadanie 1/index.html")
    .pipe(inlineCss())
    .pipe(gulp.dest("Zadanie 1/build/"));
});
